			section	.text
			global	ft_strcpy
ft_strcpy:
			test	rdi, rdi
			jz		return
			test	rsi, rsi
			jz		return
			xor		rax, rax
loop:
			mov		cl, BYTE[rsi + rax]
			mov		BYTE[rdi + rax], cl
			test	cl, cl
			jz		return
			inc		rax
			jmp		loop
return:
			mov		rax, rdi
			ret