/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 15:17:33 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/30 15:17:34 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libasm.h"
#include <stdio.h>

int	main(void)
{
	char			*test_str;
	char			*another_str;
	unsigned int	test_len;
	char			c;

	test_str = ft_strdup("This is test string.\n");
	test_len = ft_strlen(test_str);
	ft_write(1, test_str, test_len);
	another_str = ft_strdup("This is another string.\n");
	ft_write(1, another_str, ft_strlen(another_str));
	printf("ft_strcmp out: %d\n", ft_strcmp(test_str, another_str));
	fflush(stdout);
	ft_strcpy(another_str, test_str);
	printf("ft_strcmp out: %d\n", ft_strcmp(test_str, another_str));
	fflush(stdout);
	ft_read(1, &c, 1);
	ft_write(1, &c, 1);
	printf("\n");
	return (0);
}
