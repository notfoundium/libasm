			section	.text
			global	ft_write
			extern	__errno_location
ft_write:
			mov		rax, 1
			syscall
			test	rax, rax
			js		err
			ret
err:
			mov		edx, eax
			neg		edx
			push	rdx
			call    __errno_location
			pop		rdx
			mov		DWORD[rax], edx
			mov		rax, -1
			ret
