NAME		=	libasm.a
NAME_TEST	=	test

CC			=	clang
CC_FLAGS	=	-Wall -Wextra -Werror

AC			=	nasm
AC_FLAGS	=	-f elf64

SRCS		=	ft_write.s \
				ft_read.s \
				ft_strlen.s \
				ft_strcmp.s \
				ft_strcpy.s \
				ft_strdup.s

OBJS		=	$(SRCS:.s=.o)

%.o:			%.s
				$(AC) $(AC_FLAGS) $<

all:			$(NAME)

clean:
				rm -rf $(OBJS)

fclean:			clean
				rm -rf $(NAME) $(NAME_TEST)

re:				fclean $(NAME)

$(NAME):		$(OBJS)
				ar rcs $(NAME) $(OBJS)

test:			$(NAME)
				$(CC) $(CC_FLAGS) main.c  -L. -lasm -o $(NAME_TEST) 

.PHONY:			clean fclean re test
