		section	.text
		global	ft_strdup
		extern	ft_strlen
		extern	ft_strcpy
		extern	malloc
ft_strdup:
		test	rdi, rdi
		jz		err
		mov		r10, rdi
		push	r10
		call	ft_strlen
		inc		rax
		mov		rdi, rax
		call	malloc
		pop		r10
		test	rax, rax
		jz		err
		mov		rdi, rax
		mov		rsi, r10
		call	ft_strcpy
		ret
err:
		mov		rax, 0
		ret