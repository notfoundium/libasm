			section	.text
			global	ft_read
			extern	__errno_location
ft_read:
			mov		rax, 0
			syscall
			test	rax, rax
			js		err
			ret
err:
			mov		edx, eax
			neg		edx
			push	rdx
			call    __errno_location
			pop		rdx
			mov		DWORD[rax], edx
			mov		rax, -1
			ret