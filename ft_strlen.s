			section	.text
			global	ft_strlen
ft_strlen:
			xor		rax, rax
			test	rdi, rdi
			jz		err
compare:
			cmp		BYTE[rdi + rax], 0
			jnz		increment
			ret
increment:
			inc		rax
			jmp		compare
err:
			ret