/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 15:17:46 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/30 15:17:47 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_H
# define LIBASM_H

long			ft_write(int fd, const void *buf, int count);
int				ft_read(int fd, void *buf, int count);
long			ft_strlen(const char *str);
int				ft_strcmp(const char *str1, const char *str2);
char			*ft_strcpy(char *dest, const char *src);
char			*ft_strdup(const char *s);
#endif
