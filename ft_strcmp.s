			section	.text
			global	ft_strcmp
ft_strcmp:
			xor		rcx, rcx
			xor		rax, rax
			xor		rdx, rdx
			test	rdi, rdi
			jz		return_z
			test	rsi, rsi
			jz		return_z
loop:
			mov		al, BYTE[rdi + rdx]
			cmp		al, BYTE[rsi + rdx]
			jne		return
			test	al, al
			jz		return_z
			inc		rdx
			jmp		loop
return_z:
			xor		rax, rax
			ret
return:
			mov		cl, BYTE[rsi + rdx]
			sub		rax, rcx
			ret
